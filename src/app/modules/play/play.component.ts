import { Component, OnInit } from '@angular/core';
import { QuestionsService } from '../../core/services/questions.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { QuestionModel } from '../../core/state/question.model';

@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.scss']
})
export class PlayComponent implements OnInit {
  questions$ = this.questionsService.questions$;
  gettingQuestions$ = this.questionsService.gettingQuestions$;
  gameCompleted$ = this.questionsService.gameCompleted$;
  isGameComplete = false;
  correctAnswers:number = 0;
  wrongAnswers:number = 0;

  getQuestionsSubscription: Subscription = this.route.queryParams
    .pipe(switchMap(params =>
      this.questionsService.getQuestions({
        type: params.type,
        amount: params.amount,
        difficulty: params.difficulty
      })
    )).subscribe();


  constructor(
    private readonly route: ActivatedRoute,
    private readonly questionsService: QuestionsService,
  ) { }

  ngOnInit(): void {
    this.questions$.subscribe(questions => {
      const answeredQuestions = this.getAnsweredQuestions(questions);
      this.isGameComplete = answeredQuestions.length === questions.length
      this.calculateAnswers(answeredQuestions, questions);
    });
  }
  

  onAnswerClicked(questionId: QuestionModel['_id'], answerSelected: string): void {
    this.questionsService.selectAnswer(questionId, answerSelected);
  }

  private getAnsweredQuestions(questions: QuestionModel[]): QuestionModel[] {
    return questions.filter(question => !!question.selectedId);
  }

  
  private calculateAnswers(answeredQuestions: QuestionModel[], questions: QuestionModel[]): void {
    this.correctAnswers = answeredQuestions.filter(answeredQuestion =>
      answeredQuestion.answers.some(answer => answer.isCorrect && answer._id === answeredQuestion.selectedId)
    ).length;
    this.wrongAnswers = questions.length - this.correctAnswers;
  }
}
